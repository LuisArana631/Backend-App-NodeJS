// LIBRERIA PARA LEER EL ARCHIVO .ENV
require('dotenv').config()

// INSTANCIA DEL SERVIDOR EN NODEJS
const Server = require('./models/server');

const server = new Server();
server.listen();