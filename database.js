const mongoose = require('mongoose');

let URI = process.env.DB_CNN;

mongoose.connect(URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true
})
.then(db => console.log('DB connected'))
.catch(error => console.error(error));

module.exports = mongoose;