const { Router } = require('express');
const { getData, bulkLoad } =  require('../controller/catalog');
const router = Router();

// RUTA PARA EXTRAER CATALOGO
router.get('/', getData);

// RUTA PARA CARGA MASIVA
router.post('/', bulkLoad);

module.exports = router;