const { Response, Request } = require('express');
const Catalog = require('../models/catalog');

// GET ALL CATALOG
const getData = (req = Request, res = Response) => {
    const catalog = await Catalog.find();
    res.json(catalog);
}

const bulkLoad = (req = Request, res = Response) => {
    console.log('adio chula');
}

module.exports = {
    getData,
    bulkLoad
}