const mongoose = require('mongoose');
const { Schema } = mongoose;

// ESQUEMA DE LA BASE DE DATOS PARA EL CATALOGO
const CatalogSchema = new Schema({
    id: { type: String, required: true }
});

module.exports = mongoose.model('Catalog', CatalogSchema);