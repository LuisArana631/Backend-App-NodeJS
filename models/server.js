// LIBRERIAS
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const { moongose } = require('../database');

// CLASE CONTROLADORA DEL SERVIDOR
class Server {
    constructor(){
        this.app = express();
        this.port = process.env.PORT;
        this.catalogPath = '/api/catalog';
        
        var corsOptions = {
            origin: true,
            optionSuccessStatus: 200
        };

        this.app.use(cors(corsOptions));
        this.app.use(bodyParser.json({
            limit: '10mb',
            extended: true
        }));

        this.app.use(bodyParser.urlencoded({
            limit: '10mb',
            extended: true
        }));

        this.routes();
    }

    routes(){
        this.app.use(this.catalogPath, require('../routes/catalog'));
    }

    listen(){
        this.app.listen(this.port, () => {
            console.log('Servidor nodeJS levantado en el puerto: ', this.port);
        });
    }
}

module.exports = Server;